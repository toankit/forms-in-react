import React from 'react';
export default class Forms extends React.Component
{
    state=
    {
    firstName:'',
    lastName :'',
    username:'',
    password:''
    }
 onSubmit = e =>
{   e.preventDefault(); 
    this.props.onSubmit(this.state);
    console.log(this.state);
    this.setState({
    firstName:'',
    lastName :'',
    username:'',
    password:''
    })
}   
change=e=>{
this.setState({
    [e.target.name]:e.target.value
})
} 
render()
{
    return(
<div>
<form>
    FirstName: <input name="firstName"
    placeholder="Enter First Name"
    onChange={e=> this.change(e)}
    value={this.state.firstName}  /><br /><br />
    LastName: <input placeholder="Last Name"
    value={this.state.lastName}
    onChange={e=> this.setState({lastName:e.target.value})} /><br /><br />
    Username: <input placeholder="Username" 
    value={this.state.username} 
    onChange={e=>this.setState({username:e.target.value})} /><br /><br />
    Password: <input type="password" placeholder="Password" 
    value={this.state.password}
    onChange={e=> this.setState({password:e.target.value})} /> <br /><br />
    <button onClick={e=> this.onSubmit(e)}> Submit</button>
    </form>

</div>

    );
}



}