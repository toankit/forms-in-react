import React, { Component } from 'react';
import './App.css';
import Forms from './Forms';

class App extends Component {
  
    constructor(props) {
      super(props);
      this.state = {
        value: ['a'],
        textvalue : "",
        test:"",
        fields:{}
        
      }
      this.handleAddTodoItem = this.handleAddTodoItem.bind(this)
      this.handleChange = this.handleChange.bind(this)
      this.handledelTodoItem = this.handledelTodoItem.bind(this)
    }
    handleChange(e) {
      this.setState({
        textvalue:e.target.value
      })
      
      
    }
    handleAddTodoItem() {
      this.state.value.push(this.state.textvalue)
      
      this.setState(this.state)
      console.log(this.state.value)
     }
    handledelTodoItem(key){
      console.log(key);
      delete this.state.value[key]
      this.setState({
        value:this.state.value
      })
      console.log(this.state.value)
    }
    onSubmit=fields=>
    {
      console.log('App Got Fields',fields);
      this.setState({fields})
    }
    render() {
      let { value } = this.state;
      return (
        <div className='App'>
          <input type="text" placeholder="enter text" className="text" onChange={ this.handleChange } />
          <button className="buttonAdd" onClick={this.handleAddTodoItem}>Add Todo Item</button>
          {value.map((v,key) => {
            return <div><h1 className="font"><button className="allbutton" onClick={this.handledelTodoItem.bind(this, key)}>x</button>&nbsp;{v}</h1></div>
            
          })}
          <Forms onSubmit={fields=>this.onSubmit(fields)}/>
          <p>{JSON.stringify(this.state.fields)}</p>
        </div>
      )
    }
  }
  
  export default App;