import React from 'react';
import ReactDOM from 'react-dom';
import Todo from './Todo';
import './index.css';

ReactDOM.render(
  <Todo msg='This is from props'/>,
  document.getElementById('root')
);
